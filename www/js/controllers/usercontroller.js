(function() {

    angular
        .module('starterApp')
        .controller('UserController', ['userService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', UserController]);


    function UserController(userService, $mdSidenav, $mdBottomSheet, $log, $q) {
        var self = this;

        self.selected = null;
        self.users = [];
        self.selectUser = selectUser;
        self.toggleList = toggleUsersList;
        self.share = share;


        userService
            .loadAllUsers()
            .then(function(users) {
                self.users = [].concat(users);
                self.selected = users[0];
            });

        function toggleUsersList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function() {
                $mdSidenav('left').toggle();
            });
        }

        function selectUser(user) {
            self.selected = angular.isNumber(user) ? $scope.users[user] : user;
            self.toggleList();
        }


        function share($event) {
            var user = self.selected;

            $mdBottomSheet.show({
                parent: angular.element(document.getElementById('content')),
                templateUrl: 'templates/contactSheet.html',
                controller: ['$mdBottomSheet', UserSheetController],
                controllerAs: "vm",
                bindToController: true,
                targetEvent: $event
            }).then(function(clickedItem) {
                clickedItem && $log.debug(clickedItem.name + ' clicked!');
            });

            /**
             * Bottom Sheet controller for the Avatar Actions
             */
            function UserSheetController($mdBottomSheet) {
                this.user = user;
                this.items = [{
                    name: 'Phone',
                    icon: 'phone',
                    icon_url: 'img/svg/phone.svg'
                }, {
                    name: 'Twitter',
                    icon: 'twitter',
                    icon_url: 'img/svg/twitter.svg'
                }, {
                    name: 'Google+',
                    icon: 'google_plus',
                    icon_url: 'img/svg/google_plus.svg'
                }, {
                    name: 'Hangout',
                    icon: 'hangouts',
                    icon_url: 'img/svg/hangouts.svg'
                }];
                this.performAction = function(action) {
                    $mdBottomSheet.hide(action);
                };
            }
        }

    }

})();
